# Woocommerce Role Changer

> Woocommerce User Type Change Plugin for OnlineIAS

## Description

The Woocommerce Role Changer plugin allows you to convert the customer role to a student role for orders processed in Woocommerce.

## Installation

1. Download the plugin zip file from the [Plugin URI](https://synamaticscodestudio.gitlab.io/code/wordpress/woocommerce-role-changer).
2. Extract the contents of the zip file.
3. Upload the plugin folder to the `wp-content/plugins/` directory of your WordPress installation.
4. Activate the plugin through the WordPress admin dashboard.

## Usage

Once the plugin is activated, it will automatically convert the customer role to a student role for orders that have a processing status in Woocommerce.

## License

This plugin is licensed under the [GPL-2.0](https://www.gnu.org/licenses/gpl-2.0.html) license.

## Author

- Author: Vishnu Raj
- Author Email: synamatics@gmail.com
- Author URI:  https://synamaticscodestudio.gitlab.io/

# Technical Documentation

## Plugin Structure
The Woocommerce Role Changer plugin consists of the following files:

- `woocommerce-role-changer.php`: The main plugin file that initializes the plugin and registers the necessary hooks.
- `README.md`: The documentation file that provides information about the plugin, installation instructions, and usage guidelines.

## Plugin Hooks
The plugin utilizes the following WordPress hooks:

- `woocommerce_order_status_processing`: This hook is used to trigger the `woo_order_convert_paying_customer` function when an order's status is set to "processing" in Woocommerce.

## Functions
The plugin includes the following functions:

### `woo_order_convert_paying_customer( int $order_id ) : void`

This function is responsible for converting the customer role to a student role for an order.

**Parameters:**
- `$order_id` (int): The ID of the order to convert.

**Return Type:** `void`

**Description:**
This function retrieves the order object using the `$order_id` parameter and then retrieves the user associated with the order. If the user ID is greater than 0, the function updates the user meta to indicate that they are a paying customer and modifies the user's roles by removing the 'customer' role and adding the 'student' role.

## Installation
To install the Woocommerce Role Changer plugin, follow these steps:

1. Download the plugin zip file from the Plugin URI.
2. Extract the contents of the zip file.
3. Upload the plugin folder to the `wp-content/plugins/` directory of your WordPress installation.
4. Activate the plugin through the WordPress admin dashboard.

## Usage
Once the plugin is activated, it will automatically convert the customer role to a student role for orders that have a processing status in Woocommerce.

## License
This plugin is licensed under the GPL-2.0 license.

## Author
- Author: Vishnu Raj
- Author Email: synamatics@gmail.com
- Author URI: [https://synamaticscodestudio.gitlab.io/](https://synamaticscodestudio.gitlab.io/)

Feel free to customize and expand upon this technical documentation to suit your specific needs.