<?php 
/**
 * Plugin Name: Woocommerce Role Changer
 * Plugin URI:  https://synamaticscodestudio.gitlab.io/code/wordpress/woocommerce-role-changer
 * Description: Woocommerce User Type Change Plugin for OnlineIAS
 * Version:     1.2
 * Author:      Vishnu Raj
 * Author Email: synamatics@gmail.com
 * Author URI:  https://synamaticscodestudio.gitlab.io/
 * License:     GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 */

/**
 * Convert order customer role to student role.
 *
 * @param int $order_id The order ID.
 * @return void
 */
function woo_order_convert_paying_customer( int $order_id ) : void {

    $order = wc_get_order( $order_id );
    $user = $order->get_user();
    $user_id = $order->get_user_id();
    if ( $user_id > 0 ) {
        update_user_meta( $user_id, 'paying_customer', 1 );
        $user = new WP_User( $user_id );
        $user->remove_role( 'customer' ); 
        $user->add_role( 'student' );
    }
}
add_action( 'woocommerce_order_status_processing', 'woo_order_convert_paying_customer' );